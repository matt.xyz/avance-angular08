import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-tarjeta-credito',
  templateUrl: './tarjeta-credito.component.html',
  styleUrls: ['./tarjeta-credito.component.css']
})
export class TarjetaCreditoComponent implements OnInit {

  listTarjetas: any[] = [
    {titular: 'Jhon Phillip', numeroTarjeta: '134645633', fechaExpiracion: '12/26', cvv: '123'},
    {titular: 'Mia Link', numeroTarjeta: '352414145', fechaExpiracion: '04/24', cvv: '321'}
  ];

  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      titular:[''],
      numeroTarjeta: [''],
      fechaExpiracion: [''],
      cvv: ['']
    })
  }

  ngOnInit(): void {
  }

  agregarTarjeta(){
    console.log(this.form);
    
  }

}
